﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BlackBullStore.Models.ViewModels;
using BlackBullStore.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace BlackBullStore.Controllers
{
    //[Produces("application/json")]
    //[Route("Account/[controller]")]
    public class AccountController : Controller
    {
        private UserContext userDb;
        public AccountController(UserContext context)
        {
            userDb = context;
        }

        [HttpGet]
        public IActionResult Login ()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login (LoginModel model)
        {
            if(ModelState.IsValid)
            {
                User user = userDb.Users.FirstOrDefault(u => u.Login == model.Login && u.Password == model.Password);
                if(user != null)
                {
                    await Authenticate(model.Login);
                    return RedirectToAction("Values", "Index");
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Register ()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register (RegisterModel model)
        {
            if(ModelState.IsValid)
            {
                User user = userDb.Users.FirstOrDefault(u => u.Login == model.Login);
                if (user == null)
                {
                    userDb.Users.Add(new User { Login = model.Login, Password = model.Password });
                    await userDb.SaveChangesAsync();
                    await Authenticate(model.Login);
                    return RedirectToAction("Values","Index");
                }
                else
                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        private async Task Authenticate(string username)
        {
            var claims = new List<Claim>
            {
                new Claim (ClaimsIdentity.DefaultNameClaimType, username)
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout ()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Values","Index");
        }
    }
}
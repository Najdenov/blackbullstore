﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BlackBullStore.Models;
using Microsoft.EntityFrameworkCore;

namespace BlackBullStore.Controllers
{
    //[Route("values/[controller]")]
    public class ValuesController : Controller
    {
        public class PurchaseData
        {
            public string OrderNumber { get; set; }
            public string Email { get; set; }
            public string MobileNumber { get; set; }
            public string Delivery { get; set; }
            public string Address { get; set; }
            public int [] Id { get; set; }
            public int [] Quantity { get; set; }
        }
        
        StoreContext db;
        public ValuesController(StoreContext context)
        {
            db = context;
        }

        // GET api/values
        //[HttpGet]
        public IActionResult Index() //public IEnumerable<string> Get()
        {
            return View(); //new string[] { "value1", "value2" };
        }

        public IActionResult About()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Buy ()
        {
            //List<Store> Stores = db.Stores.Include(s => s.Blend).ToList();
            return View(db.Blends.Include(s => s.Stores).ToList());
        }

        [HttpPost]  
        public IActionResult Buy (PurchaseData data)
        {
            //Формирование таблицы Order базы BlackBullStore
            Order order = new Order();
            order.Address = data.Address;
            order.Delivery = string.IsNullOrEmpty(data.Delivery) ? false : true;
            order.Email = data.Email;
            order.MobileNumber = int.Parse(data.MobileNumber);
            order.OrderNumber = int.Parse(data.OrderNumber);
            order.Status = OrderStatus.New;

            db.Orders.Add(order);
            db.SaveChanges();

            //float totalCost = 0;

            ContentResult cr = new ContentResult();

            //Формирование таблицы OrderDetail и внесение корректировок в таблицу Store

            for (int i = 0; i < data.Id.Length; i++)
            {
                if (data.Quantity[i] != 0)
                {
                    Store store = db.Stores.FirstOrDefault(s => s.BlendId == data.Id[i]);
                    if (store.Quantity < data.Quantity[i])
                    {
                        cr.ContentType = "text/html";
                        cr.StatusCode = 200;
                        cr.Content = "В таких колличествах товар: " + db.Blends.FirstOrDefault(b => b.Id == store.BlendId).ToString() + " отсутствует";
                        return cr;
                    }
                    OrderDetail od = new OrderDetail();

                    od.OrderId = db.Orders.FirstOrDefault(o => o.OrderNumber == order.OrderNumber).Id;
                    od.BlendId = data.Id[i];
                    store.Quantity = store.Quantity - data.Quantity[i];
                    od.Quantity = data.Quantity[i];
                    db.Stores.Update(store);
                    db.OrderDetails.Add(od);
                }
            }
            db.SaveChanges();
            cr.Content = "Спасибо за покупку";
            cr.ContentType = "text/html";
            cr.StatusCode = 200;
            return cr;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

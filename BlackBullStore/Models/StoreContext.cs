﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlackBullStore.Models
{
    public class StoreContext : DbContext
    {
        public DbSet<Blend> Blends { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<Store> Stores { get; set; }

        public StoreContext(DbContextOptions<StoreContext> options) :base (options) 
            { }

        protected override void OnModelCreating (ModelBuilder modelBuilder)
        {
            // base.OnModelCreating(
            //modelBuilder.Entity<OrderDetail>().HasOne(p => p.Blend).WithMany(p => p.OrderDetails);
        }
        
    }
}
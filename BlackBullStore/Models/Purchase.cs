﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBullStore.Models
{
    public class Purchase
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateOfPurchase { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public float Cost { get; set; }
        public int BlendId { get; set; }
        public PurchaseStatus Status { get; set; }
        public bool isPaid { get; set; }
        public Blend Blend { get; set; }
    }
}

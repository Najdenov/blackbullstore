﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBullStore.Models
{
    public class Blend
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        public int Arabica { get; set; }
        public int Robusta { get; set; }

        [Required]
        public float Cost { get; set; }
        public ICollection<Store> Stores { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
        public ICollection<Purchase> Purchases { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBullStore.Models
{
    public enum OrderStatus
    {   New, Processing, Delivery, Down }
    
    public enum PurchaseStatus
    {
        New, Complete   }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBullStore.Models
{
    public class OrderDetail
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int BlendId { get; set; }

        [Required]
        public int Quantity { get; set; }
        public Order Order { get; set; }
        public Blend Blend { get; set; }
    }
    
}

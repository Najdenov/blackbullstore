﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBullStore.Models
{
    public class Store
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int BlendId { get; set; }

        [Required]
        public int Quantity { get; set; }
        public Blend Blend { get; set; }
    }
}

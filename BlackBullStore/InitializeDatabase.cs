﻿using BlackBullStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackBullStore
{
    public static class InitializeDatabase
    {
        public static void Initialize(StoreContext context)
        {
            Blend b1 = new Blend { Name = "A60/R40", Arabica = 60, Robusta = 40, Cost = 24 };
            Blend b2 = new Blend { Name = "A50/R50", Arabica = 50, Robusta = 50, Cost = 18 };
            Blend b3 = new Blend { Name = "A40/R60", Arabica = 40, Robusta = 60, Cost = 12 };

            if(!context.Blends.Any())
            {
                context.Blends.AddRange(b1, b2, b3);
                context.SaveChanges();
            }

            if(!context.Purchases.Any())
            {
                Purchase pur1 = new Purchase();
                pur1.DateOfPurchase = new DateTime(2017, 10, 30);
                pur1.Blend = b1;
                pur1.Quantity = 100;
                pur1.Status = PurchaseStatus.Complete;
                pur1.isPaid = true;
                pur1.Cost = 20;

                Purchase pur2 = new Purchase();
                pur2.DateOfPurchase = new DateTime(2017, 10, 20);
                pur2.Blend = b2;
                pur2.Quantity = 200;
                pur2.Status = PurchaseStatus.Complete;
                pur2.isPaid = true;
                pur2.Cost = 15;

                Purchase pur3 = new Purchase();
                pur3.DateOfPurchase = new DateTime(2017, 10, 10);
                pur3.Blend = b3;
                pur3.Quantity = 200;
                pur3.Status = PurchaseStatus.Complete;
                pur3.isPaid = true;
                pur3.Cost = 10;
                context.Purchases.AddRange(pur1, pur2, pur3);
                context.SaveChanges();
            }

            if(!context.Stores.Any())
            {
                context.Stores.AddRange(new Store
                {
                    Quantity = 100,
                    Blend = b1 //context.Blends.Select(blend => blend).Where(blend => blend.Id == 4).First()
                },
                new Store
                {
                    Quantity = 200,
                    Blend = b2 //context.Blends.Select(blend => blend).Where(blend => blend.Id == 5).First()
                },
                new Store
                {
                    Quantity = 300,
                    Blend = b3 //context.Blends.Select(blend => blend).Where(blend => blend.Id == 6).First()
                });

                context.SaveChanges();
            }
        }
    }
}
